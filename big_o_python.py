import time

# notes regarding big 0 notation
def time_it(func): # decorator to time function call
    def wrapper(*args, **kwargs):
        tic = time.perf_counter()
        func(*args, **kwargs)
        toc = time.perf_counter()
        print(f'Time elapsed {toc-tic}')
    return wrapper

## Constant Time
@time_it
def take_first(my_list):
    '''
    This represents constant time, b/c we always return the
    first value no matter what
    0(1)
    '''
    return my_list[0]

## Linear Time
@time_it
def find_max(my_list):
    '''
    This function represents linear time, this iterates over
    every single element in the array.
    0(N)
    '''
    for i in range(len(my_list)):
        max_value = my_list[0]
        if my_list[i] > max_value:
            max_value = my_list[i]
    
    return max_value

@time_it
def element_multiplier(my_list):
    '''
    Length of list impacts the outer loop and inner loop
    O(N**2)
    '''
    for i in range(len(my_list)):
        for j in range(len(my_list)):
            x = my_list[i] * my_list[j]



if __name__ == "__main__":
    long_list = [i for i in range(0,1000)]
    short_list = [i for i in range(0,5)]


    # take_first(long_list)

    # take_first(short_list)

    # find_max(long_list)
    
    # find_max(short_list)

    element_multiplier(short_list)
    element_multiplier(long_list)

    ## Common runtimes
    '''
    O(1)       | Constant    | first item in list
    O(N)       | Linear      | search linearly through list
    O(N**2)    | Polynomial  | loop nested within loop
    O(log N)   | Logarithmic | binary search
    O(2**N)    | Exponential | count combinations of list elements
    O(N log N) |             | mergesort, quicksort
    O(N!)      | factorial   | generate all permutations of a list or generating all possible gps routes to a location
    '''

    '''
    O(log N) : picture searching through a list, and with each iteration of the search
    we jump the middle continuously to find element within sorted list.
    we are doubling the list size (taking more space) but still running 1 "action"
    '''
